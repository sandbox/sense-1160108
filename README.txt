README.txt
==========

Filter target blank from content and replace with rel attribute
Opening of new tab / window is provided by javascript.


INSTALL
=======

1. Go to admin/build/modules
2. Enable Target blank filter module
3. Go to admin/settings/filters and enable for each input format


AUTHOR
======
Sven Culley -> http://www.sense-design.de
