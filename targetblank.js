Drupal.behaviors.targetblank = function(context) {
  $('a[rel=new]').click( function() {
    window.open(this.href);
    return false;
  });
}